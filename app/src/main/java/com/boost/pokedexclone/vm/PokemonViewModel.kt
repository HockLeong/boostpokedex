package com.boost.pokedexclone.vm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.boost.pokedexclone.data.network.ApiService
import com.boost.pokedexclone.data.repository.PokemonRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class PokemonViewModel(application: Application) :
    AndroidViewModel(application) {

    private val context = getApplication<Application>().applicationContext

    // Create a new Job
    private val parentJob = Job()

    // Create a coroutine context with the job and the dispatcher
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Default

    // Create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)
    private val pokemonRepository: PokemonRepository = PokemonRepository(ApiService.pokemonService)

    // Pokemon live data
    val pokemonLiveData = MutableLiveData<PokemonListResponse>()

    // Live data of pokemon details
    val pokemonDetailsLiveData = MutableLiveData<PokemonDetailsResponse>()

    // Live data that store all your favourite pokemon(s)
    val pokemonFavouriteLiveData = MutableLiveData<MutableList<PokemonListResponse.Result>>()

    fun getPokemonList() {
        scope.launch {
            val pokemonList = pokemonRepository.getPokemonList()
            pokemonLiveData.postValue(pokemonList)
        }
    }

    fun getPokemonDetails(name: String) {
        scope.launch {
            val pokemonDetails = pokemonRepository.getPokemonDetails(name)
            pokemonDetailsLiveData.postValue(pokemonDetails)
        }
    }

    fun getFavouritePokemonList() {
        val favouriteList = pokemonRepository.getFavouritePokemonList(context)
        pokemonFavouriteLiveData.postValue(favouriteList)
    }

    fun saveFavouritePokemon(data: PokemonListResponse.Result) {
        pokemonRepository.saveFavouritePokemon(context, data)

        // Update live data after adding new favourite pokemon
        getFavouritePokemonList()
    }

    fun removeFavouritePokemon(data: PokemonListResponse.Result) {
        pokemonRepository.removeFavouritePokemon(context, data)

        // Update live data after removing from favourite list
        getFavouritePokemonList()
    }
}
