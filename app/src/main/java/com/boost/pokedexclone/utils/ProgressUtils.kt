package com.boost.pokedexclone.utils

import android.app.AlertDialog
import android.content.Context
import android.view.Window
import android.widget.ProgressBar

object ProgressUtils {
    private lateinit var alertDialog: AlertDialog
    private lateinit var pDialog: ProgressBar

    fun showLoading(context: Context) {
        pDialog = ProgressBar(context)

        alertDialog = AlertDialog.Builder(context)
            .setCancelable(false)
            .setView(pDialog)
            .create()

        // Transparent background goes here
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.show()
    }

    fun hideLoading() {
        try {
            if (alertDialog.isShowing) {
                alertDialog.dismiss()
            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }
}
