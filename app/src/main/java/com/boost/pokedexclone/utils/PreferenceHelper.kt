package com.boost.pokedexclone.utils

import android.content.Context
import com.boost.pokedexclone.constant.Preferences
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

object PreferenceHelper {
    fun getFavouritePokemonList(context: Context): MutableList<PokemonListResponse.Result> {
        val key = PreferenceUtils.defaultPrefs(context).getString(Preferences.FAVOURITE, "")
        if (key == "") {
            return mutableListOf()
        }

        val mutableListPokemonType =
            object : TypeToken<MutableList<PokemonListResponse.Result>>() {}.type
        return GsonBuilder().create().fromJson(key, mutableListPokemonType)
    }

    fun saveFavouritePokemon(context: Context, data: PokemonListResponse.Result) {
        val favouriteList = getFavouritePokemonList(context)

        favouriteList.add(data)
        val favouriteJson = GsonBuilder().create().toJson(favouriteList)

        PreferenceUtils.defaultPrefs(context)
            .edit()
            .putString(Preferences.FAVOURITE, favouriteJson)
            .apply()
    }

    fun removeFavouritePokemon(context: Context, data: PokemonListResponse.Result){
        val favouriteList = getFavouritePokemonList(context)

        favouriteList.remove(data)
        val favouriteJson = GsonBuilder().create().toJson(favouriteList)

        PreferenceUtils.defaultPrefs(context)
            .edit()
            .putString(Preferences.FAVOURITE, favouriteJson)
            .apply()
    }
}
