package com.boost.pokedexclone.data.network

import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonApi {
    @GET("pokemon?limit=9999")
    suspend fun getPokemonList(): Response<PokemonListResponse>

    @GET("pokemon/{name}")
    suspend fun getPokemonDetail(@Path("name") name: String?): Response<PokemonDetailsResponse>
}
