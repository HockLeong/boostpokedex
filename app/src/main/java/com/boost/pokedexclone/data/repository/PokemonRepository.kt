package com.boost.pokedexclone.data.repository

import android.content.Context
import com.boost.pokedexclone.base.BaseRepository
import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.boost.pokedexclone.data.network.PokemonApi
import com.boost.pokedexclone.utils.PreferenceHelper

class PokemonRepository(private val apiInterface: PokemonApi) : BaseRepository() {
    suspend fun getPokemonList(): PokemonListResponse? {
        return safeApiCall(
            call = { apiInterface.getPokemonList() },
            errorMessage = "Error fetching pokemon list"
        )
    }

    suspend fun getPokemonDetails(name: String): PokemonDetailsResponse? {
        return safeApiCall(
            call = { apiInterface.getPokemonDetail(name) },
            errorMessage = "Error fetching pokemon details"
        )
    }

    fun getFavouritePokemonList(context: Context): MutableList<PokemonListResponse.Result> {
        return PreferenceHelper.getFavouritePokemonList(context)
    }

    fun saveFavouritePokemon(context: Context, data: PokemonListResponse.Result) {
        PreferenceHelper.saveFavouritePokemon(context, data)
    }

    fun removeFavouritePokemon(context: Context, data: PokemonListResponse.Result) {
        PreferenceHelper.removeFavouritePokemon(context, data)
    }
}
