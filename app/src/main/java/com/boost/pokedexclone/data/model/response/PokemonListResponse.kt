package com.boost.pokedexclone.data.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PokemonListResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: String,
    @SerializedName("previous")
    val previous: Any,
    @SerializedName("results")
    val results: List<Result>
) : Serializable {
    data class Result(
        @SerializedName("name")
        val name: String,
        @SerializedName("url")
        val url: String
    ) : Serializable
}
