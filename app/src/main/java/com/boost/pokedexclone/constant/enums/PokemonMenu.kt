package com.boost.pokedexclone.constant.enums

enum class PokemonMenu {
    POKEMON,
    FAVOURITE,
}
