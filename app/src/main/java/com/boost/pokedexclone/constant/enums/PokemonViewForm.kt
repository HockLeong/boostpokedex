package com.boost.pokedexclone.constant.enums

enum class PokemonViewForm {
    LIST_VIEW,
    GRID_VIEW,
}
