package com.boost.pokedexclone.base

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.ui_toolbar.*

open class BaseActivity : AppCompatActivity() {
    fun setToolBarTitle(title: String) {
        setSupportActionBar(ui_toolbar)

        if (supportActionBar != null) {
            toolbar_title.text = title
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            ui_toolbar.setNavigationOnClickListener { super@BaseActivity.onBackPressed() }
        }
    }
}
