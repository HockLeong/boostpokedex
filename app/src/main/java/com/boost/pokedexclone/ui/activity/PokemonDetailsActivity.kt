package com.boost.pokedexclone.ui.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.boost.pokedexclone.R
import com.boost.pokedexclone.base.BaseActivity
import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.boost.pokedexclone.ui.adapter.PokemonAbilitiesAdapter
import com.boost.pokedexclone.ui.adapter.PokemonMovesAdapter
import com.boost.pokedexclone.ui.adapter.PokemonTypesAdapter
import com.boost.pokedexclone.vm.PokemonViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_pokemon_details.*

class PokemonDetailsActivity : BaseActivity() {
    private lateinit var pokemonViewModel: PokemonViewModel
    private lateinit var pokemon: PokemonListResponse.Result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_details)

        setToolBarTitle(getString(R.string.pokemon_details_title))
        pokemon = intent.getSerializableExtra("pokemonObject") as PokemonListResponse.Result

        initView()
        initViewModel()
    }

    private fun initView() {
        rvTypes.layoutManager = GridLayoutManager(this, 2)
        rvTypes.isNestedScrollingEnabled = false

        rvMoves.layoutManager = GridLayoutManager(this, 2)
        rvMoves.isNestedScrollingEnabled = false

        rvAbilities.layoutManager = GridLayoutManager(this, 2)
        rvAbilities.isNestedScrollingEnabled = false
    }

    private fun initViewModel() {
        pokemonViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)
        ).get(PokemonViewModel::class.java)

        pokemonViewModel.getPokemonDetails(pokemon.name)
        pokemonViewModel.pokemonDetailsLiveData.observe(this, Observer {
            if (it == null) {
                return@Observer
            }

            setPokemonDetails(it)
        })

        pokemonViewModel.getFavouritePokemonList()
        pokemonViewModel.pokemonFavouriteLiveData.observe(this, Observer {
            if (it == null) {
                return@Observer
            }

            setFavouriteAction(it)
        })
    }

    private fun setPokemonDetails(data: PokemonDetailsResponse) {
        Glide.with(this)
            .load(data.sprites.frontDefault)
            .into(ivPokemon)

        tvPokemonNo.text = "# ${data.id}"
        tvPokemonName.text = data.name
        tvWeight.text = "Weight: ${data.weight}"
        tvHeight.text = "Height: ${data.height}"

        rvTypes.adapter = PokemonTypesAdapter(this, data.types)
        rvAbilities.adapter = PokemonAbilitiesAdapter(this, data.abilities)
        rvMoves.adapter = PokemonMovesAdapter(this, data.moves)
    }

    private fun setFavouriteAction(favouriteList: MutableList<PokemonListResponse.Result>) {
        val isFavourite: Boolean = favouriteList.any { it.name == pokemon.name }

        if (isFavourite) {
            Glide.with(this)
                .load(R.mipmap.favorite)
                .into(ivFavourite)

            ivFavourite.setOnClickListener {
                pokemonViewModel.removeFavouritePokemon(pokemon)
            }
        } else {
            Glide.with(this)
                .load(R.mipmap.unfavourite)
                .into(ivFavourite)

            ivFavourite.setOnClickListener {
                pokemonViewModel.saveFavouritePokemon(pokemon)
            }
        }
    }
}
