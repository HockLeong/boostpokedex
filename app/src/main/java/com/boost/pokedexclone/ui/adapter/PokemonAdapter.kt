package com.boost.pokedexclone.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.boost.pokedexclone.R
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.boost.pokedexclone.ui.activity.PokemonDetailsActivity
import kotlinx.android.synthetic.main.item_pokemon.view.*
import java.io.Serializable

class PokemonAdapter(
    private val context: Context,
    private var list: MutableList<PokemonListResponse.Result>
) : RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {

    fun update(list: MutableList<PokemonListResponse.Result>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]

        holder.tvName.text = item.name
        holder.cvPokemon.setOnClickListener {
            val intent = Intent(context, PokemonDetailsActivity::class.java)
            intent.putExtra("pokemonObject", item as Serializable)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cvPokemon = itemView.cvPokemon!!
        val tvName = itemView.tvPokemonName!!
    }
}
