package com.boost.pokedexclone.ui.activity

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.boost.pokedexclone.R
import com.boost.pokedexclone.base.BaseActivity
import com.boost.pokedexclone.constant.enums.PokemonMenu
import com.boost.pokedexclone.ui.adapter.HomeAdapter
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        init()
    }

    private fun init() {
        val menuList = PokemonMenu.values().toMutableList()

        rvMenu.isNestedScrollingEnabled = false
        rvMenu.layoutManager = GridLayoutManager(this, 2)
        rvMenu.adapter = HomeAdapter(menuList, this)
    }
}
