package com.boost.pokedexclone.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.boost.pokedexclone.R
import com.boost.pokedexclone.constant.enums.PokemonMenu
import com.boost.pokedexclone.ui.activity.PokemonListActivity
import kotlinx.android.synthetic.main.item_menu.view.*

class HomeAdapter(
    private val list: MutableList<PokemonMenu>,
    private val context: Context
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]

        when (item) {
            PokemonMenu.POKEMON -> setCardBackgroundColor(holder, R.color.lightRed)
            PokemonMenu.FAVOURITE -> setCardBackgroundColor(holder, R.color.teal_200)
        }

        holder.tvMenuName.text = item.name
        holder.cvMenu.setOnClickListener {
            val intent = Intent(context, PokemonListActivity::class.java)
            intent.putExtra("type", item)
            context.startActivity(intent)
        }
    }

    private fun setCardBackgroundColor(holder: ViewHolder, colorRes: Int) {
        holder.cvMenu.setCardBackgroundColor(ContextCompat.getColor(context, colorRes))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cvMenu = itemView.cvMenu!!
        val tvMenuName = itemView.tvMenuName!!
    }
}
