package com.boost.pokedexclone.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.boost.pokedexclone.R
import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import kotlinx.android.synthetic.main.item_move.view.*

class PokemonMovesAdapter(
    private val context: Context,
    private val list: List<PokemonDetailsResponse.Move>
) : RecyclerView.Adapter<PokemonMovesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_move, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position].move
        holder.name.text = item.name
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.tvMove!!
    }
}
