package com.boost.pokedexclone.ui.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.boost.pokedexclone.R
import com.boost.pokedexclone.base.BaseActivity
import com.boost.pokedexclone.constant.enums.PokemonMenu
import com.boost.pokedexclone.constant.enums.PokemonViewForm
import com.boost.pokedexclone.data.model.response.PokemonListResponse
import com.boost.pokedexclone.ui.adapter.PokemonAdapter
import com.boost.pokedexclone.utils.ProgressUtils
import com.boost.pokedexclone.vm.PokemonViewModel
import com.leinardi.android.speeddial.SpeedDialView
import kotlinx.android.synthetic.main.activity_pokemon_list.*

class PokemonListActivity : BaseActivity() {
    // Variable used for configure pokemon listing or favourite page
    private lateinit var pokemonMenu: PokemonMenu

    // Variable to determine type of the layout display
    private var layoutType: PokemonViewForm = PokemonViewForm.LIST_VIEW

    private lateinit var pokemonViewModel: PokemonViewModel
    private lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var pokemonList: MutableList<PokemonListResponse.Result>
    private lateinit var pokemonReferenceList: MutableList<PokemonListResponse.Result>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_list)

        pokemonMenu = intent.getSerializableExtra("type") as PokemonMenu

        when (pokemonMenu) {
            PokemonMenu.POKEMON -> setToolBarTitle(getString(R.string.pokemon_list_tile))
            else -> setToolBarTitle(getString(R.string.pokemon_favourite_tile))
        }

        initView()
        initViewModel()
    }

    private fun initView() {
        rvPokemon.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )

        sdvPokemon.inflate(
            when (pokemonMenu) {
                PokemonMenu.POKEMON -> R.menu.menu_pokemon
                else -> R.menu.menu_favourite
            }
        )

        sdvPokemon.setOnActionSelectedListener(SpeedDialView.OnActionSelectedListener { actionItem ->
            if (pokemonList.isEmpty()) {
                return@OnActionSelectedListener true
            }

            when (actionItem.id) {
                // Switching between list/grid view
                R.id.menuLayout -> {
                    setRecyclerViewLayoutManager()
                }
                // Sort by number
                R.id.menuNumber -> {
                    pokemonList.clear()
                    pokemonList.addAll(pokemonReferenceList)
                    pokemonAdapter.update(pokemonList)
                }
                // Sort by alphabet
                R.id.menuAlphabet -> {
                    pokemonList.sortBy { it.name }
                    pokemonAdapter.update(pokemonList)
                }
            }

            sdvPokemon.close()
            return@OnActionSelectedListener true
        })
    }

    private fun initViewModel() {
        pokemonViewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)
        ).get(PokemonViewModel::class.java)

        pokemonViewModel.pokemonLiveData.observe(this, Observer {
            // Hide progress dialog
            ProgressUtils.hideLoading()

            if (it == null) {
                llNoResult.visibility = View.VISIBLE
                rvPokemon.visibility = View.GONE
                return@Observer
            }

            llNoResult.visibility = View.GONE
            rvPokemon.visibility = View.VISIBLE
            setPokemonInfo(it.results.toMutableList())
        })

        pokemonViewModel.pokemonFavouriteLiveData.observe(this, Observer {
            if (it == null || it.isEmpty()) {
                llNoResult.visibility = View.VISIBLE
                rvPokemon.visibility = View.GONE
                return@Observer
            }

            llNoResult.visibility = View.GONE
            rvPokemon.visibility = View.VISIBLE
            setPokemonInfo(it)
        })
    }

    /**
     * Call API for pokemon list
     */
    private fun getPokemonList() {
        // Show progress dialog when call for API
        ProgressUtils.showLoading(this)
        pokemonViewModel.getPokemonList()
    }

    /**
     * Get the favourite pokemon added from shared preference
     */
    private fun getFavouritePokemonList() {
        pokemonViewModel.getFavouritePokemonList()
    }

    private fun setPokemonInfo(data: MutableList<PokemonListResponse.Result>) {
        pokemonList = data

        // Init and assign original list to reference list for sort by 9number
        pokemonReferenceList = mutableListOf()
        pokemonReferenceList.addAll(pokemonList)

        pokemonAdapter = PokemonAdapter(this, pokemonList)
        rvPokemon.adapter = pokemonAdapter
    }

    override fun onResume() {
        super.onResume()

        when (pokemonMenu) {
            PokemonMenu.POKEMON -> getPokemonList()
            else -> getFavouritePokemonList()
        }
    }

    private fun setRecyclerViewLayoutManager() {
        var scrollPosition = 0
        val layoutManager: RecyclerView.LayoutManager

        // If a layout manager has already been set, get current scroll position.
        if (rvPokemon.layoutManager != null) {
            scrollPosition = (rvPokemon.layoutManager as LinearLayoutManager)
                .findFirstCompletelyVisibleItemPosition()
        }

        when (layoutType) {
            PokemonViewForm.LIST_VIEW -> {
                layoutManager = GridLayoutManager(this, 2)
                layoutType = PokemonViewForm.GRID_VIEW
            }
            else -> {
                layoutManager = LinearLayoutManager(
                    this,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                layoutType = PokemonViewForm.LIST_VIEW
            }
        }

        rvPokemon.layoutManager = layoutManager
        rvPokemon.scrollToPosition(scrollPosition)
    }
}
