package com.boost.pokedexclone.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.boost.pokedexclone.R
import com.boost.pokedexclone.data.model.response.PokemonDetailsResponse
import kotlinx.android.synthetic.main.item_ability.view.*

class PokemonAbilitiesAdapter(
    private val context: Context,
    private val list: List<PokemonDetailsResponse.Ability>
) : RecyclerView.Adapter<PokemonAbilitiesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_ability, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position].ability
        holder.name.text = item.name
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.tvAbility!!
    }
}
