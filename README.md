
# Pokedex Clone
This is a simple pokedex demo app written in Kotlin using [PokeApi](https://pokeapi.co/)

This app provides you a list with each unique pokemon and shows the details when it is selected

# Feature
- Home (Page that navigate to pokemon and favourite page)
- Pokemon (Page that display list of pokemon, it can display in list/grid view, sort by alphabet and number)
- Pokemon details (Page that display the details info of selected pokemon)
- Favourite (Page that display the pokemon you mark as favourite. You can perform favourite/unfavourite action in this page)

### Libraries & Tools Used
- [x] [Kotlin](https://kotlinlang.org/)
- [X] [MVVM](https://developer.android.com/jetpack/guide)
- [x] [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [x] [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [x] [Coroutines](https://developer.android.com/topic/libraries/architecture/coroutines)
- [X] [Gson](https://github.com/google/gson)
- [x] [Retrofit](https://square.github.io/retrofit/)
- [X] [Glide](https://bumptech.github.io/glide/)

### Folder Structure
Here is the folder structure we have been using in this project
```
com.boost.pokedex/
|- base/
|- constant/
|- data/
|- ui/
|- utils/
|- vm/
```

#### Base
Contains the base component required for the project

BaseActivity used for base setup and configuration for activities

BaseRepository used for api call for all of sub-repository class

#### Constant
Contains constant required for the project like `enums` and `preferences`

#### Data
Contains the data layer of your project, includes directories for model, network and repository.  

Model consists of all the data classes required for the project, including requests and responses

Network consists of basic setup for all of the API network call

Repository handle data operations and know where to get the data from and what API calls to make when data is updated. It acts as middleman between data sources.

#### UI
Contains all the layout required for the applications. E.g. activity, fragment, or adapter

#### Utils
Contains the utilities/common functions of your application

#### VM
This directory is where all your ViewModel stored. The VM basically handling all kinds of business logic and provides the data for a specific UI component, such as a fragment or activity
